package ru.tsc.kitaev.tm.service;

import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.IProjectTaskService;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(projectId, taskId);
    }

    @Override
    public Project removeById(final String projectId) {
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}
