package ru.tsc.kitaev.tm.api.service;

import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    Project removeById(String projectId);

}
