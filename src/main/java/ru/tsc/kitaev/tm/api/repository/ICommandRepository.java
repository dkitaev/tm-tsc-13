package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
